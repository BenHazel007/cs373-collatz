#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

cacheSize = 1000000

cache = [0] * cacheSize


# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>

    if i > j:  # swap i and j if they are in the wrong order
        t = i
        i = j
        j = t

    maxCycle = 0

    m = (j // 2) + 1  # middle step optimization

    if i < m:
        i = m

    for n in range(i, j + 1):
        cycleLen = recurse(n, 0)

        if cycleLen > maxCycle:
            maxCycle = cycleLen

    return maxCycle


def recurse(temp: int, cycleLen: int) -> int:  # recursively solves a collatz number
    global cacheSize
    global cache

    if temp is 1:  # base case of 1
        return 1
    elif (
        temp < cacheSize and cache[temp] != 0
    ):  # base case if the cycle number is already cached which skips computing more steps
        return cache[temp]
    else:
        if temp % 2 == 0:
            cycleLen += (
                recurse(temp // 2, cycleLen) + 1
            )  # finds the cycle number of the next step down and adds 1
        else:
            cycleLen += (
                recurse(temp + (temp >> 1) + 1, cycleLen) + 2
            )  # finds the cycle number of two steps down and adds 2

        if temp < cacheSize:
            cache[temp] = cycleLen

    return cycleLen


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
